#ifndef ALLOY_TYPES_HPP
#define ALLOY_TYPES_HPP

//#include <cstdint>
#include "cpp11compat.h"

int32_t find_fcc_alloy_type(int8_t* mapping, int32_t* numbers);
int32_t find_bcc_alloy_type(int8_t* mapping, int32_t* numbers);

#endif

